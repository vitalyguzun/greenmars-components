import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import history from './store/history';
import { TopMenu } from './components/TopMenu';
import { Content } from './components/content/Content';
import './styles/App.css';

const App = ({ store }) => {
    return (
        <Provider store={store}>
            <Router history={history}>
                <div className="greenmars-components">
                    <TopMenu />
                    <div className='body'>
                        <Content />
                    </div>
                </div>
            </Router>
        </Provider>
    );
}

export default App;
