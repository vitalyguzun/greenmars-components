import React from 'react';
import { NavLink } from 'react-router-dom';

import '../../styles/leftMenu.css';

export const LeftMenu = () => {
    return (
        <div className='left-menu'>
            <NavLink to='/js360-native'>
                <div>js360-native playground</div>
            </NavLink>
            <NavLink to='/emptyCard'>
                <div>EmptyCard</div>
            </NavLink>
        </div>
    );
}
