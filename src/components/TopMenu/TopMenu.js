import React from 'react';
import { withRouter } from 'react-router'
import { find, propEq, path } from 'ramda';

import '../../styles/topMenu.css';

const routes = [
    { path: '/', name: 'js360-native playground' },
    { path: '/js360-native', name: 'js360-native playground' },
    { path: '/emptyCard', name: 'EmptyCard' }
];

const getComponentName = (link) => {
    return path(['name'], find(propEq('path', link), routes));
}

export const TopMenu = withRouter(({ location: { pathname }}) => {
    return (
        <div className='top-menu'>
            <div className='company'><h3>Greenmars Components</h3></div>
            <div className='component-name'><h3>{getComponentName(pathname)}</h3></div>
        </div>
    );
});
