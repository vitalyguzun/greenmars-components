import React, { Component } from 'react';
import cx from 'classnames';
import 'antd/lib/checkbox/style/index.css';
const Checkbox = require('antd/lib/checkbox');

export default class CheckboxComponent extends Component {
    onChange = ({ target }) => {
        this.props.input.onChange(target.checked);
    }

    toggle = () => {
        const { input } = this.props;
        input.onChange(!input.value);
    }

    render () {
        const { label, text, input, meta: { active, initial }} = this.props;
        const value = input.value === '' ? initial : input.value;

        return (
            <div className={cx('antd-input', { active })}>
                { label && <label className='ant-label' onClick={this.toggle}>{ label }</label> }
                <Checkbox onChange={this.onChange} className='ant-input' checked={value}>{ text }</Checkbox>
            </div>
        );
    }
}
