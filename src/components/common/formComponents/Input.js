import React, { Component } from 'react';
import cx from 'classnames';
import 'antd/lib/select/style/index.css';
const Input = require('antd/lib/input');

export default class InputComponent extends Component {
    constructor() {
        super();
        this.input = null;
    }

    focus = () => {
        this.input.refs.input.select();
    }

    render() {
        const { input, label, addonBefore, disabled, meta: { active }} = this.props;

        return (
            <div className={cx('antd-input', { active })}>
                { label && <label className='ant-label' onClick={this.focus}>{ label }</label> }
                <Input {...input} onClick={this.focus} ref={(node) => this.input = node} addonBefore={addonBefore} disabled={disabled} />
            </div>
        );
    }
}
