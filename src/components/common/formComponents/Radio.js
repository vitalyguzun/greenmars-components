import React, { Component } from 'react';
import cx from 'classnames';
import 'antd/lib/radio/style/index.css';
const Radio = require('antd/lib/radio');
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

export default class RadioComponent extends Component {
    onChange = ({ target }) => {
        this.props.input.onChange(target.value);
    }

    render () {
        const { label, input, meta: { active }} = this.props;

        return (
            <div className={cx('antd-input', { active })}>
                { label && <label className='ant-label' onClick={this.focus}>{ label }</label> }
                <RadioGroup onChange={this.onChange} defaultValue={input.value}>
                    <RadioButton value={true}>true</RadioButton>
                    <RadioButton value={false}>false</RadioButton>
                    <RadioButton value=''>null</RadioButton>
                </RadioGroup>
            </div>
        );
    }
}
