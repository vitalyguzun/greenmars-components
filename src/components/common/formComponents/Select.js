import React from 'react';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import { path } from 'ramda';

const Select = require('antd/lib/select');
const { Option } = Select;

let SelectBefore = ({ meta, form, change }) => {
    const targetClass = path(['values', 'targetClass'], meta);

    const onChange = (val) => {
        let target = '';

        switch (val) {
            case 'nodeElement': target = `document.querySelector(${targetClass})`; break;
            case 'nodeList': target = `document.querySelectorAll(${targetClass})`; break;
            case 'selector':
            default: target = targetClass; break;
        };

        change('js360Form', 'target', target);
    }

    return (
        <Select defaultValue='selector' onChange={onChange}>
            <Option value='selector'>target as selector</Option>
            <Option value='nodeElement'>target as nodeElement</Option>
            <Option value='nodeList'>target as nodeList</Option>
        </Select>
    );
}

export default connect(({ form }) => ({ form }), { change })(SelectBefore);
