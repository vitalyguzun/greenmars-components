import React from 'react';
import { Route } from 'react-router-dom';

import JS360 from './cards/JS360/JS360';
import { EmptyCard } from './cards/EmptyCard';
import '../../styles/content.css';
import '../../styles/card.css';

export const Content = () => {
    return (
        <div className='content-component'>
            <Route path='/' component={JS360} exact />
            <Route path='/js360-native' component={JS360} />
            <Route path='/emptyCard' component={EmptyCard} />
        </div>
    );
}
