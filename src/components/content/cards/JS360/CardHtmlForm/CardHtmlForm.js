import React, { Component } from 'react';
import { Field } from 'redux-form';

import Radio from '../../../../common/formComponents/Radio';
import Input from '../../../../common/formComponents/Input';

const Collapse = require('antd/lib/collapse');
const Panel = Collapse.Panel;

export class CardHtmlForm extends Component {
    render() {
        const { name } = this.props;

        return (
            <Collapse defaultActiveKey={['1']}>
                <Panel header='JS config' key='1'>
                    <div className='fields'>
                        <div className='fields-title'>HTML атрибуты</div>
                        <Field name={`${name}.className`} component={Input} label='class' />
                        <Field name={`${name}.preview`} component={Input} label='data-preview' />
                        <Field name={`${name}.url`} component={Input} label='data-url' />
                        <Field name={`${name}.autoPlay`} component={Radio} label='data-auto-play' />
                        <Field name={`${name}.preloader`} component={Radio} label='data-preloader' />
                        <Field name={`${name}.speed`} component={Input} label='data-speed' />
                        <Field name={`${name}.retinaUrl`} component={Input} label='data-retina-url' />
                        <Field name={`${name}.loadEvents`} component={Input} label='data-load-events' />
                        <Field name={`${name}.rotateEvents`} component={Input} label='data-rotate-events' />
                    </div>
                </Panel>
            </Collapse>
        );
    };
}
