import React, { Component } from 'react';
import { Field } from 'redux-form';
import 'antd/lib/collapse/style/index.css';

import Checkbox from '../../../../common/formComponents/Checkbox';
import Input from '../../../../common/formComponents/Input';
import Select from '../../../../common/formComponents/Select';
import '../../../../../styles/ant.css';

const Collapse = require('antd/lib/collapse');
const Panel = Collapse.Panel;

export class CardJsForm extends Component {
    render() {
        return (
            <div className='js360-js-input fields'>
                <Collapse defaultActiveKey={['1']}>
                    <Panel header='JS config' key='1'>
                        <Field name={'baseUrl'} component={Input} label='baseUrl' />
                        <Field name={'target'} component={Input} addonBefore={<Select meta={this.props} />} disabled={true} />
                        <Field name={'url'} component={Input} label='url' />
                        <Field name={'autoPlay'} component={Checkbox} label='autoPlay' text={'true'} />
                        <Field name={'retinaUrl'} component={Input} label='retinaUrl' />
                        <Field name={'speed'} component={Input} label='speed' />
                        <Field name={'preloader'} component={Checkbox} label='preloader' text={'true'} />
                        <Field name={'preview'} component={Input} label='preview' />
                        <Field name={'loadEvents'} component={Input} label='loadEvents' />
                        <Field name={'rotateEvents'} component={Input} label='rotateEvents' />
                        <Field name={'controlLoad'} component={Checkbox} label='controlLoad' text={'true'} />
                        <Field name={'controlPlay'} component={Checkbox} label='controlPlay' text={'true'} />
                    </Panel>
                    <Panel header='Events' key='2'>
                        <Field name={'onLoad'} component={Checkbox} label='onLoad' text={'function'} />
                        <Field name={'onRotateStart'} component={Checkbox} label='onRotateStart' text={'function'} />
                        <Field name={'onRotate'} component={Checkbox} label='onRotate' text={'function'} />
                        <Field name={'onRotateEnd'} component={Checkbox} label='onRotateEnd' text={'function'} />
                        <Field name={'onPlayStart'} component={Checkbox} label='onPlayStart' text={'function'} />
                        <Field name={'onPlay'} component={Checkbox} label='onPlay' text={'function'} />
                        <Field name={'onPlayEnd'} component={Checkbox} label='onPlayEnd' text={'function'} />
                    </Panel>
                    <Panel header='Methods' key='3'>
                        <Field name={'load'} component={Checkbox} label='load' text={'function'} />
                        <Field name={'play'} component={Checkbox} label='play' text={'function'} />
                        <Field name={'stop'} component={Checkbox} label='stop' text={'function'} />
                        <Field name={'toggle'} component={Checkbox} label='toggle' text={'function'} />
                    </Panel>
                    <Panel header='Flags' key='4'>
                        <Field name={'isLoaded'} component={Checkbox} label='isLoaded' text={'function'} />
                        <Field name={'isPending'} component={Checkbox} label='isPending' text={'function'} />
                        <Field name={'isMoving'} component={Checkbox} label='isMoving' text={'function'} />
                        <Field name={'isStopped'} component={Checkbox} label='isStopped' text={'function'} />
                        <Field name={'isPlayed'} component={Checkbox} label='isPlayed' text={'function'} />
                    </Panel>
                </Collapse>
            </div>
        );
    };
}
