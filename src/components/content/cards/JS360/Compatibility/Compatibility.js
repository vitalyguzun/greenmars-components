import React, { Component } from 'react';

import Rows from './rows';
import '../../../../../styles/Compatibility.css';

export class Compatibility extends Component {
    render () {
        return (
            <div className="compatibility">
                <h2>Проверено на следующих устройствах</h2>
                <table>
                    <thead>
                        <tr>
                            <th colSpan='2'>Свойство</th>
                            <th colSpan='8'>Desktop browsers</th>
                            <th colSpan='9'>Mobile browsers</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className='browsers'>
                            <td>Js config</td>
                            <td>HTML data</td>
                            <td>Chrome Mac</td>
                            <td>Chrome Windows</td>
                            <td>Mozilla Mac</td>
                            <td>Mozilla Windows</td>
                            <td>Safari Mac</td>
                            <td>Safari Windows</td>
                            <td>Opera Mac</td>
                            <td>Opera Windows</td>
                            <td>Chrome Android</td>
                            <td>Chrome Ios</td>
                            <td>Firefox Android</td>
                            <td>Firefox Ios</td>
                            <td>Safari Android</td>
                            <td>Safari Ios</td>
                            <td>Opera Android</td>
                            <td>Opera Ios</td>
                            <td>Default Android</td>
                        </tr>
                        <Rows.Target />
                        <Rows.BaseUrl />
                        <Rows.Url />
                        <Rows.Preview />
                        <Rows.AutoPlay />
                        <Rows.Preloader />
                        <Rows.Speed />
                        <Rows.RetinaUrl />
                        <Rows.LoadEvents />
                        <Rows.RotateEvents />
                        <Rows.ControlLoad />
                        <Rows.ControlPlay />
                    </tbody>
                </table>
            </div>
        );
    }
}
