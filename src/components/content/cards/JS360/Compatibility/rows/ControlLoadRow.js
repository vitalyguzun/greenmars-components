import React from 'react';

export default () => (
    <tr>
        <td className='js-config'>controlLoad</td>
        <td className='html-data'>data-control-load</td>
        <td className='green chrome-mac'>yes</td>
        <td className='green chrome-windows'>yes</td>
        <td className='green mozilla-mac'>yes</td>
        <td className='green mozilla-windows'>yes</td>
        <td className='green safari-mac'>yes</td>
        <td className='red safari-windows'>no</td>
        <td className='green opera-mac'>yes</td>
        <td className='green opera-windows'>yes</td>
        <td className='green chrome-android'>yes</td>
        <td className='green chrome-ios'>yes</td>
        <td className='green firefox-android'>yes</td>
        <td className='green firefox-ios'>yes</td>
        <td className='green safari-android'>yes</td>
        <td className='green safari-ios'>yes</td>
        <td className='green opera-android'>yes</td>
        <td className='green opera-ios'>yes</td>
        <td className='red default-android'>no</td>
    </tr>
)
