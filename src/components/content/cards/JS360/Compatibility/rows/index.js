import Target from './TargetRow';
import BaseUrl from './BaseUrlRow';
import Url from './UrlRow';
import Preview from './PreviewRow';
import AutoPlay from './AutoPlayRow';
import Preloader from './PreloaderRow';
import Speed from './SpeedRow';
import RetinaUrl from './RetinaUrlRow';
import RotateEvents from './RotateEventsRow';
import LoadEvents from './LoadEventsRow';
import ControlLoad from './ControlLoadRow';
import ControlPlay from './ControlPlayRow';

export default {
    Target,
    BaseUrl,
    Url,
    Preview,
    AutoPlay,
    Preloader,
    Speed,
    RetinaUrl,
    RotateEvents,
    LoadEvents,
    ControlLoad,
    ControlPlay
}
