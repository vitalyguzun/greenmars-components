import React, { Component } from 'react';

const isDefined = (value) => value !== undefined && value !== '';
const Attribute = ({ name, text }) => {
    return (
        <span>
            <br/>
            <span className='keyword'>  {`data-${name}`}</span>
            <span className='punctuation'>=</span>
            <span className='value-string'>{`'${text}'`}</span>
        </span>
    );
}

export class HtmlSource extends Component {
    render() {
        const { name, values } = this.props;

        const {
            rotateEvents,
            loadEvents,
            className,
            retinaUrl,
            preloader,
            autoPlay,
            preview,
            speed,
            url
        } = values[name];

        return (
            <div className='html-source'>
                <pre className='source'>
                    <span>{'<'}</span><span className='tag'>div</span>
                    { className &&
                        <span>
                            <span className='keyword'> {'class'}</span>
                            <span className='punctuation'>=</span>
                            <span className='value-string'>{`'${className}'`}</span>
                        </span>
                    }

                    { preview && <Attribute name='preview' text={preview} /> }
                    { url && <Attribute name='url' text={url} /> }
                    { isDefined(autoPlay) && <Attribute name='autoPlay' text={autoPlay} /> }
                    { isDefined(preloader) && <Attribute name='preloader' text={preloader} /> }
                    { speed && <Attribute name='speed' text={speed} /> }
                    { retinaUrl && <Attribute name='retina-url' text={retinaUrl} /> }
                    { loadEvents && <Attribute name='load-events' text={loadEvents} /> }
                    { rotateEvents && <Attribute name='rotate-events' text={rotateEvents} /> }

                    <span>{'>'}</span>
                    <span>{'<'}</span>
                    <span className='tag'>/div</span>
                    <span>{'>'}</span>
                </pre>
            </div>
        );
    }
}
