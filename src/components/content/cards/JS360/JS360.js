import React, { Component } from 'react';
import { connect } from 'react-redux';
import { JS360 } from 'js360';
import { reduxForm } from 'redux-form';
import { path } from 'ramda';
import 'js360/style.css';

// import { JS360 } from '../../../js360';
import { CardHtmlForm } from './CardHtmlForm';
import { CardJsForm } from './CardJsForm';
import { JSSource } from './JSSource';
import { Compatibility } from './Compatibility';
import '../../../../styles/Source.css';
import '../../../../styles/js360.css';
import 'antd/lib/button/style/index.css';

const Button = require('antd/lib/button');

const jsConfig = {
    baseUrl: './assets',
    target: '.js360-canvas',
    loadEvents: ['mousemove', 'touchmove'],
    rotateEvents: ['mousedown', 'touchstart'],
    autoPlay: true,
    preloader: true,
    controlLoad: true,
    controlPlay: true
};

const htmlCards = [{
    name: 'product_1',
    className: 'js360',
    preview: './assets/product_2.jpeg',
    url: 'product_2.json',
    baseUrl: './assets'
}];

const form = 'js360Form';

const getInitialState = (htmlCards) => {
    const initialValues = {};

    htmlCards.forEach(({ className, preview, url, autoPlay, name }) => {
        initialValues[name] = { className, preview, url, autoPlay };
    });

    for (let key in jsConfig) {
        initialValues[key] = jsConfig[key] || null;
    }

    initialValues.targetClass = `'${jsConfig.target}'`;

    return initialValues;
}

class Js360 extends Component {
    constructor() {
        super();
        this.js360 = null;
        this.state = { cards: htmlCards };
    }

    componentDidUpdate() {
        const { target, ...nextConfig } = this.props.meta[form].values;

        if (this.js360 && this.js360.isPlaying()) this.js360.stop();

        this.js360 = new JS360({ ...jsConfig, ...nextConfig });
        this.js360.render();
    }

    renderCards = () => {
        const { cards } = this.state;
        const values = path([form, 'values'], this.props.meta) || {};

        return cards.map((card, index) => {
            const {
                rotateEvents,
                loadEvents,
                retinaUrl,
                preloader,
                className,
                autoPlay,
                preview,
                speed,
                name,
                url
            } = { ...card, ...values[card.name]};

            return (
                <div className='js360-card' key={index}>
                    <div className='preview'>
                        <div className={`js360-canvas ${className}`}
                            data-preview={preview}
                            data-url={url}
                            data-rotate-events={rotateEvents}
                            data-load-events={loadEvents}
                            data-auto-play={autoPlay}
                            data-preloader={preloader}
                            data-speed={speed}
                            data-retina-url={retinaUrl}></div>
                    </div>
                    <CardHtmlForm name={name} />
                </div>
            )}
        );
    }

    render () {
        const { meta } = this.props;

        return (
            <div>
                <div className='js360-playground'>
                    <form className='js360-input'>
                        <div className='js360-html-input'>
                            { this.renderCards() }
                            <div className='controls-container'>
                                <Button type='primary' className='add'><span>+</span></Button>
                            </div>
                        </div>
                        <CardJsForm {...meta[form]} />
                    </form>
                    <JSSource {...meta[form]} cards={this.state.cards} />
                </div>
                <Compatibility />
            </div>
        );
    }
}

Js360 = reduxForm({ form, destroyOnUnmount: false })(Js360);

const stateToProps = ({ form }) => {
    return {
        meta: form,
        initialValues: getInitialState(htmlCards)
    };
}

export default connect(stateToProps)(Js360);
