import React, { Component } from 'react';

import { HtmlSource } from '../HtmlSource';

const Property = ({ name, text, textClass = 'string' }) => {
    const quote = textClass === 'string' ? '\'' : '';

    return (
        <span>
            <span className='punctuation'>,</span>
            <br/>
            <span className='object-property'>  { name }</span>
            <span className='punctuation'>: </span>
            <span className={`value-${textClass}`}>{`${quote}${text}${quote}`}</span>
        </span>
    );
}

const Event = ({ name, text }) => {
    return (
        <span>
            <span className='punctuation'>,</span>
            <br/>
            <span className='object-property'>  { name }</span>
            <span className='punctuation'>:</span>
            <span className='punctuation'> () =></span>
            <span className='value-object'> console</span>
            <span className='punctuation'>.</span>
            <span className='object-property'>log</span>
            <span className='punctuation'>(</span>
            <span className='value-string'>'{ text }'</span>
            <span className='punctuation'>)</span>
        </span>
    );
}

const Method = ({ method, comment }) => {
    return (
        <span>
            <br/>
            { comment &&
                <span className='comments'>
                    <br/>
                    <span>{`// ${comment}`}</span>
                    <br/>
                </span>
            }
            <span className='value-object'>js360</span>
            <span className='punctuation'>.</span>
            <span className='object-property'>{ method }</span>
            <span className='punctuation'>();</span>
        </span>
    );
}

export class JSSource extends Component {
    constructor() {
        super();

        this.isStylesNeeded = null;
    }

    renderCSSSource = () => {
        return (
            <pre className='source'>
                <span className='comments'>{'//'} CSS</span>
                <br/>
                <span className='value-object'>.js360</span>
                <span> {'{'}</span>
                <br/>
                <span className='value-string'>  width: </span>
                <span className='keyword'>580px;</span>
                <br/>
                <span>{'}'}</span>
            </pre>
        );
    }

    renderHtmlSource = () => {
        const { cards, values } = this.props;

        return (
            <pre className='source'>
                <span className='comments'>{'//'} HTML</span>
                <br/>
                { cards.map(({ name }, key) => {
                    const preloader = values ? values[`${name}-html-preloader`]: null;
                    this.isStylesNeeded = preloader;

                    return values ? <HtmlSource values={values} name={name} key={key} /> : null;
                })}
            </pre>
        );
    }

    renderJSSource = () => {
        const { values = {} } = this.props;

        // Config
        const {
            controlLoad,
            controlPlay,
            rotateEvents,
            loadEvents,
            preloader,
            retinaUrl,
            autoPlay,
            baseUrl,
            preview,
            target,
            speed,
            url
        } = values;

        // Events
        const {
            onLoad,
            onRotateStart,
            onRotate,
            onRotateEnd,
            onPlayStart,
            onPlay,
            onPlayEnd
        } = values;

        // Methods
        const { load, play, stop, toggle } = values

        return (
            <pre className='source'>
                <div className='comments'>{'//'} JS</div>
                <div>
                    <span className='keyword'>import</span>
                    <span className='punctuation'> {'{'}</span>
                    <span className='value-object'> JS360</span>
                    <span className='punctuation'> {'}'}</span>
                    <span className='keyword'> from</span>
                    <span className='value-string'> 'js360'</span>
                    <span className='punctuation'>;</span>
                </div>
                { (preloader || controlLoad || controlPlay || this.isStylesNeeded) &&
                    <div>
                        <span className='keyword'>import</span>
                        <span className='value-string'> 'js360/style.css'</span>
                        <span className='punctuation'>;</span>
                    </div>
                }
                <br/>
                <div>
                    <span className='keyword'>const</span>
                    <span className='value-object'> js360</span>
                    <span className='punctuation'> =</span>
                    <span className='keyword'> new</span>
                    <span className='value-object'> JS360</span>
                    <span className='punctuation'>{'({'}</span>
                </div>
                <div>
                    <span className='object-property'>  baseUrl</span>
                    <span className='punctuation'>:</span>
                    <span className='value-string'> '{ baseUrl }'</span>
                    <span className='punctuation'>,</span>
                </div>
                <span className='object-property'>  target</span>
                <span className='punctuation'>:</span>
                <span className='value-string'> { target }</span>
                { url && <Property name='url' text={ url } /> }
                { autoPlay && <Property name='autoPlay' textClass='boolean' text={true} /> }
                { preloader && <Property name='preloader' textClass='boolean' text={true} /> }
                { retinaUrl && <Property name='retinaUrl' text={ retinaUrl } /> }
                { speed && <Property name='speed' textClass='number' text={ speed } /> }
                { preview && <Property name='preview' text={ preview } /> }
                { loadEvents && <Property name='loadEvents' textClass='array' text={ loadEvents.map((e) => `"${e}"`) } /> }
                { rotateEvents && <Property name='rotateEvents' textClass='array' text={ rotateEvents.map((e) => `"${e}"`) } /> }
                { controlLoad && <Property name='controlLoad' textClass='boolean' text={ controlLoad } /> }
                { controlPlay && <Property name='controlPlay' textClass='boolean' text={ controlPlay } /> }

                { onLoad        && <Event name={'onload'} text={'content is loaded'}/> }
                { onRotateStart && <Event name={'onRotateStart'} text={'rotation is started'}/> }
                { onRotate      && <Event name={'onRotate'} text={'rotation is happening now'}/> }
                { onRotateEnd   && <Event name={'onRotateEnd'} text={'rotation is finished'}/> }
                { onPlayStart   && <Event name={'onPlayStart'} text={'autoPlay is started'}/> }
                { onPlay        && <Event name={'onPlay'} text={'autoPlay is happening now'}/> }
                { onPlayEnd     && <Event name={'onPlayEnd'} text={'autoPlay is finished'}/> }

                <div className='punctuation'>{'});'}</div>
                <Method method={'render'} />

                { load && <Method method={'load'} comment='программная загрузка контента' /> }
                { play && <Method method={'play'} comment='программный запуск автоматического просмотра' /> }
                { stop && <Method method={'stop'} comment='программная остановка автоматического просмотра' /> }
                { toggle && <Method method={'toggle'} comment='программное переключение режимов play / stop' /> }
            </pre>
        );
    }

    render () {
        return (
            <div className='js360-source'>
                <div className='title'>Код</div>
                { this.renderCSSSource() }
                { this.renderHtmlSource() }
                { this.renderJSSource() }
            </div>
        );
    }
}
