import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import Store from './store/configureStore';
import './styles/index.css';

ReactDOM.render(<App store={Store} />, document.getElementById('root'));
registerServiceWorker();
