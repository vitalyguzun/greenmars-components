import { reducer as form } from 'redux-form';

const rootReducer = {
    form
};

export default rootReducer;
