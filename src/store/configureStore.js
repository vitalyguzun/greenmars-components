import { applyMiddleware, combineReducers, createStore } from 'redux';
import reduxThunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { routerMiddleware } from 'react-router-redux';

import history from './history';
import rootReducer from '../reducers/rootReducer';

let middlewares = [reduxThunk, routerMiddleware(history)];

if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger({collapsed: true}));
}

const Store = createStore(
    combineReducers({ ...rootReducer }),
    undefined,
    applyMiddleware(...middlewares)
);

export default Store;
